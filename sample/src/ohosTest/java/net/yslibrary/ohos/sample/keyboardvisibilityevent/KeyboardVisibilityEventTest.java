/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.yslibrary.ohos.sample.keyboardvisibilityevent;


import net.yslibrary.ohos.sample.EventHelper;
import net.yslibrary.ohos.sample.keyboardvisibilityevent.*;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.Component;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class KeyboardVisibilityEventTest {
    Ability ability;
    private final int screenHeight = 2340;

    @Before
    public void setUp() throws Exception {
        ability = (Ability) EventHelper.startAbility(MainAbility.class);
        Thread.sleep(2000);
    }

    @After
    public void tearDown() throws Exception {
        Thread.sleep(2000);
        EventHelper.clearAbilities();
    }

    @Test
    public void testGetInstance() {
        Assert.assertNotNull("TestgetInstance not is null ", KeyboardVisibilityEvent.getInstance());
    }

    @Test
    public void testIsKeyboardVisible() throws Exception {
        Component component = new Component(ability.getContext());
        KeyboardVisibilityEvent.getInstance().setAbilitySliceRoot(component);
        boolean valueState = KeyboardVisibilityEvent.getInstance().isKeyboardVisible(ability);
        Assert.assertFalse("TestisKeyboardVisible is State ", valueState);
    }

    @Test
    public void testGetRealHeight() {
        Assert.assertEquals("TestgetRealHeight Screen Height is ", (int) KeyboardVisibilityEvent.getInstance().getRealHeight(ability.getContext()), screenHeight);
    }

}