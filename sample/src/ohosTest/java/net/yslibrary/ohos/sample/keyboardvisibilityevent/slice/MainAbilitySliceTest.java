/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.yslibrary.ohos.sample.keyboardvisibilityevent.slice;

import net.yslibrary.ohos.sample.EventHelper;
import net.yslibrary.ohos.sample.keyboardvisibilityevent.*;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.miscservices.timeutility.Time;
import ohos.multimodalinput.event.TouchEvent;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MainAbilitySliceTest {

    private Ability ability = null;
    private boolean mKeyBoardValue = false;
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    private TextField textField = null;
    private Text text = null;
    private Button button = null;
    private Component rootView;

    @Before
    public void setUp() throws Exception {
        ability = EventHelper.startAbility(MainAbility.class);
        Thread.sleep(2000);
        textField = (TextField) ability.findComponentById(ResourceTable.Id_textfield);
        text = (Text) ability.findComponentById(ResourceTable.Id_keyboard_status);
        button = (Button) ability.findComponentById(ResourceTable.Id_btn_unregister);
        rootView = (Component) ability.findComponentById(ResourceTable.Id_root);
    }

    @After
    public void tearDown() throws Exception {
        Thread.sleep(2000);
        EventHelper.clearAbilities();
        textField = null;
        mKeyBoardValue = false;
        textField = null;
        text = null;
        button = null;
    }

    @Test
    public void testMainAbilityAddKeyBoardListener() throws Exception {

        KeyboardVisibilityEvent keyboardVisibilityEvent = new KeyboardVisibilityEvent();

        keyboardVisibilityEvent.setAbilitySliceRoot(rootView);
        Unregistrar unregistrar = keyboardVisibilityEvent.registerEventListener(ability, new KeyboardVisibilityEventListener() {
            @Override
            public void onVisibilityChanged(boolean isOpen) {
                mKeyBoardValue = isOpen;
            }
        });

        simulateClickOnScreen(textField);
        Thread.sleep(1000);
        Assert.assertTrue("testMainAbilityAddKeyBoardListener add KeyBoardListener", mKeyBoardValue);
        unregistrar.unregister(rootView);
    }

    @Test
    public void testMainAbilityRemoveKeyBoardListener() throws Exception {
        String oldState = text.getText();
        simulateClickOnScreen(button);
        Thread.sleep(1000);
        simulateClickOnScreen(textField);
        Thread.sleep(1000);

        String newState = text.getText();
        Assert.assertTrue("testMainAbilityAddKeyBoardListener Remove KeyBoardListener", oldState.equals(newState));

    }

    @Test
    public void testMainAbilityKeyBoardShow() throws Exception {
        String oldvalue = text.getText();
        simulateClickOnScreen(textField);
        Thread.sleep(2000);
        String newvalue = text.getText();
        Assert.assertFalse("MainAbilityKeyBoardShow state", oldvalue.equals(newvalue));
    }

    private void simulateClickOnScreen(Component component) {
        int[] pos = component.getLocationOnScreen();
        pos[0] += component.getWidth() / 2;
        pos[1] += component.getHeight() / 2;

        long downTime = Time.getRealActiveTime();
        long upTime = downTime;
        TouchEvent ev1 = EventHelper.obtainTouchEvent(downTime, downTime, EventHelper.ACTION_DOWN, pos[0], pos[1]);
        sAbilityDelegator.triggerTouchEvent(ability, ev1);
        TouchEvent ev2 = EventHelper.obtainTouchEvent(downTime, upTime, EventHelper.ACTION_UP, pos[0], pos[1]);
        sAbilityDelegator.triggerTouchEvent(ability, ev2);
    }

}