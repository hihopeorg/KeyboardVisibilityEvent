package net.yslibrary.ohos.sample.keyboardvisibilityevent.slice;

import net.yslibrary.ohos.sample.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.ohos.sample.keyboardvisibilityevent.KeyboardVisibilityEventListener;
import net.yslibrary.ohos.sample.keyboardvisibilityevent.ResourceTable;
import net.yslibrary.ohos.sample.keyboardvisibilityevent.Unregistrar;
import net.yslibrary.ohos.sample.keyboardvisibilityevent.util.UIUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.window.service.Window;
import ohos.agp.window.service.WindowManager;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private Text keyboardStatus;
    private TextField textField;
    private Button buttonUnregister;
    private Unregistrar unregistrar;
    private KeyboardVisibilityEvent mKeyboardVisibilityEvent;

    Component root_View;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        keyboardStatus = (Text) findComponentById(ResourceTable.Id_keyboard_status);
        textField = (TextField) findComponentById(ResourceTable.Id_textfield);
        buttonUnregister = (Button) findComponentById(ResourceTable.Id_btn_unregister);
        buttonUnregister.setClickedListener(this::onClick);

        root_View = findComponentById(ResourceTable.Id_root);
      /*
          You can also use {@link KeyboardVisibilityEvent#setEventListener(AbilitySlice, KeyboardVisibilityEventListener)}
          if you don't want to unregister the event manually.
         */


        mKeyboardVisibilityEvent = new KeyboardVisibilityEvent();
        mKeyboardVisibilityEvent.setAbilitySliceRoot(root_View);
        unregistrar = mKeyboardVisibilityEvent.registerEventListener(this.getAbility(), new KeyboardVisibilityEventListener() {
            @Override
            public void onVisibilityChanged(boolean isOpen) {
                updateKeyboardStatusText(isOpen);
            }
        });

        Window window = getWindow();
        window.setInputPanelDisplayType(WindowManager.LayoutConfig.INPUT_ADJUST_PAN);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_btn_unregister:
                unregister();
                break;
        }

    }

    private void updateKeyboardStatusText(boolean isOpen) {
        if (isOpen) {
            keyboardStatus.setText("keyboard is visible-->" + isOpen);
        } else {
            keyboardStatus.setText("keyboard is hidden-->" + isOpen);
        }
    }
    private void unregister() {
        unregistrar.unregister(root_View);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregistrar.unregister(root_View);
    }
}
