# KeyboardVisibilityEvent

**本项目是基于开源项目KeyboardVisibilityEvent进行ohos的移植和开发的，可以通过项目标签以及github地址（https://github.com/yshrsmz/KeyboardVisibilityEvent ）追踪到原项目版本**

#### 项目介绍

- 项目名称：软键盘状态检测。
 - 所属系列：ohos的第三方组件适配移植
 - 功能：支持检查软键盘状态。
 - 项目移植状态：完成
 - 调用差异：无
 - 项目作者和维护人：hihope
 - 联系方式：hihope@hoperun.com
 - 原项目Doc地址：https://github.com/yshrsmz/KeyboardVisibilityEvent
 - 编程语言：Java
 - 外部库依赖：无
 - 原项目基线版本：v3.0.0 , sha1:0c7838a5c76bc8d037b10dea0b9217f6d1a6ae3b
#### 演示效果

<img src="gif/demo.gif"/>

#### 安装教程

方法1.

1. 下载动画库jar包KeyboardVisibilityEvent.har。
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下jar包的引用。
4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法2.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址

```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```

1. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
dependencies {
   implementation 'net.yslibrary.ohos:keyboardvisibilityevent:1.0.1'
}
```

 #### 使用说明

 ```java
 
 private Unregistrar unregistrar;
 private KeyboardVisibilityEvent mKeyboardVisibilityEvent;
 
mKeyboardVisibilityEvent = new KeyboardVisibilityEvent();
        mKeyboardVisibilityEvent.setAbilitySliceRoot(root_View);
        unregistrar = mKeyboardVisibilityEvent.registerEventListener(this, new KeyboardVisibilityEventListener() {
            @Override
            public void onVisibilityChanged(boolean isOpen) {
                updateKeyboardStatusText(isOpen);
            }
        });
        
 ```
### 版本迭代

 - v1.0.1

 #### 版权和许可信息
 - Apache Licence
