package net.yslibrary.ohos.sample.keyboardvisibilityevent;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentTreeObserver;

import java.lang.ref.WeakReference;
/**
 * @author Anoop S S
 * anoopvvs@gmail.com
 * on 28/02/2017
 */
public class SimpleUnregistrar implements Unregistrar {
    private WeakReference<Ability> activityWeakReference;
    private WeakReference<Component.LayoutRefreshedListener> onGlobalLayoutListenerWeakReference;

    public SimpleUnregistrar(Ability ability, Component.LayoutRefreshedListener globalLayoutListener) {
        activityWeakReference = new WeakReference(ability);
        onGlobalLayoutListenerWeakReference = new WeakReference(globalLayoutListener);
    }

    @Override
    public void unregister(Component rootview) {
        Ability ability=activityWeakReference.get();
        Component.LayoutRefreshedListener layoutRefreshedListener=onGlobalLayoutListenerWeakReference.get();
         if (null!=ability&&null!=layoutRefreshedListener){
             rootview.setLayoutRefreshedListener(null);
         }
        activityWeakReference.clear();
        onGlobalLayoutListenerWeakReference.clear();
    }
}
