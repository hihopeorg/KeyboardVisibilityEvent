package net.yslibrary.ohos.sample.keyboardvisibilityevent;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilityLifecycleCallbacks;
import ohos.utils.PacMap;
/**
 * Created by Piasy{github.com/Piasy} on 8/18/16.
 */

public abstract class AutoActivityLifecycleCallback implements AbilityLifecycleCallbacks {
    private Ability mAbility;
    public  AutoActivityLifecycleCallback(Ability ability) {
        this.mAbility=ability;
    }

    @Override
    public void onAbilityStart(Ability ability) {

    }

    @Override
    public void onAbilityActive(Ability ability) {

    }

    @Override
    public void onAbilityInactive(Ability ability) {

    }

    @Override
    public void onAbilityForeground(Ability ability) {

    }

    @Override
    public void onAbilityBackground(Ability ability) {

    }

    @Override
    public void onAbilityStop(Ability ability) {
        //当前ability销毁是执行
        System.out.println("onAbilityStop is run ");
        if (ability == mAbility){
            onTargetActivityDestroyed();
        }
    }

    @Override
    public void onAbilitySaveState(PacMap pacMap) {

    }
    public abstract void onTargetActivityDestroyed();
}
