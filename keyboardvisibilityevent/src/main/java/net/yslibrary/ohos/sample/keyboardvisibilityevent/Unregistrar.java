package net.yslibrary.ohos.sample.keyboardvisibilityevent;

import ohos.agp.components.Component;
/**
 * @author Anoop S S
 * anoopvvs@gmail.com
 * on 28/02/2017
 */
public interface Unregistrar {
    /**
     * unregisters the [ViewTreeObserver.OnGlobalLayoutListener] and there by does not provide any more callback to the [KeyboardVisibilityEventListener]
     */
    void  unregister(Component RootView);
}
