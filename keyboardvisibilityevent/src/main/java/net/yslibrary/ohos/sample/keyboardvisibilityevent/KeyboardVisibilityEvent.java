package net.yslibrary.ohos.sample.keyboardvisibilityevent;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.LifecycleObserver;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentTreeObserver;
import ohos.agp.utils.Point;
import ohos.agp.utils.Rect;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

public class KeyboardVisibilityEvent {
    private static final double KEYBOARD_MIN_HEIGHT_RATIO = 0.15;
    private Component mRootView;
    private LifecycleObserver mLifecycleObserver = null;
    private static KeyboardVisibilityEvent keyboardVisibilityEvent;

    public static KeyboardVisibilityEvent getInstance() {
        if (keyboardVisibilityEvent == null) {
            keyboardVisibilityEvent = new KeyboardVisibilityEvent();
        }
        return keyboardVisibilityEvent;
    }

    /**
     * Set keyboard visibility change event listener.
     * This automatically remove registered event listener when the Activity is destroyed
     *
     * @param ability ability
     * @param listener KeyboardVisibilityEventListener
     */

    public void setEventListener(
            Ability ability,
            KeyboardVisibilityEventListener listener
    ) {
        Unregistrar unregistrar = registerEventListener(ability, listener);
        //需要生命周期的监听  ILifecycleObserver
        mLifecycleObserver = new LifecycleObserver() {
            @Override
            public void onStop() {
                super.onStop();
                ability.getLifecycle().removeObserver(this);
                unregistrar.unregister(mRootView);
            }
        };
        ability.getLifecycle().addObserver(mLifecycleObserver);
    }

    /**
     * Set keyboard visibility change event listener.
     * This automatically remove registered event listener when the Activity is destroyed
     *
     * @param ability Activity
     * @param listener KeyboardVisibilityEventListener
     */

    public Unregistrar registerEventListener(
            Ability ability,
            KeyboardVisibilityEventListener listener) {
        if (ability == null) {
            throw new NullPointerException("Parameter:AbilitySlice must not be null");
        }
        if (listener == null) {
            throw new NullPointerException("Parameter:listener must not be null");
        }
        if (mRootView == null) {
            throw new NullPointerException("mRootView:listener must not be null");
        }


        Component.LayoutRefreshedListener layoutListener = new Component.LayoutRefreshedListener() {
            private boolean wasOpened = false;

            @Override
            public void onRefreshed(Component component) {
                boolean isOpen = isKeyboardVisible(ability);

                if (isOpen != wasOpened) {
                    // keyboard state has not changed

                    wasOpened = isOpen;
                    listener.onVisibilityChanged(isOpen);
                }
            }
        };
        mRootView.setLayoutRefreshedListener(layoutListener);
        return new SimpleUnregistrar(ability, layoutListener);
    }

    /**
     * Determine if keyboard is visible
     *
     * @param ability Activity
     * @return Whether keyboard is visible or not
     */
    public boolean isKeyboardVisible(Ability ability) {
        Rect rect = new Rect();

        mRootView.getSelfVisibleRect(rect);

        int[] location = mRootView.getLocationOnScreen();

        int screenHeight = (int) getRealHeight(ability.getContext());
        int heightDiff = screenHeight - rect.getHeight() - location[1];

        if (location[1] == 0) {
            return false;
        } else {
            return heightDiff > screenHeight * KEYBOARD_MIN_HEIGHT_RATIO;
        }

    }


    //获取根界面进行传入
    public void setAbilitySliceRoot(Component RootView) {
        this.mRootView = RootView;
    }

    public Component getAbilitySliceRoot() {
        if (mRootView == null) {
            throw new NullPointerException("mRootView:listener must not be null");
        } else {
            return mRootView;
        }
    }

    public static float getRealHeight(Context context) {
        Point point = new Point();
        DisplayManager.getInstance().getDefaultDisplay(context).get().getRealSize(point);
        return point.getPointY();
    }


}
