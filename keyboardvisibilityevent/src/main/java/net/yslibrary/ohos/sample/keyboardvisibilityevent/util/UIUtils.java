package net.yslibrary.ohos.sample.keyboardvisibilityevent.util;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.TextField;
import ohos.agp.utils.Point;
import ohos.agp.utils.Rect;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.lang.reflect.Method;
/**
 * Created by yshrsmz on 15/03/17.
 */
public class UIUtils {
    private static UIUtils uiUtils;

    public static UIUtils  getInstance(){
        if (uiUtils==null){
            uiUtils=new UIUtils();
        }
        return uiUtils;
    }

    /**
     * Show keyboard and focus to given EditText
     *
     * @param context Context
     * @param target  TextField to focus
     */
    public  boolean  showKeyboard(Context context, TextField target){
        try {
            Class inputClass = Class.forName("ohos.miscservices.inputmethod.InputMethodController");
            Method method = inputClass.getMethod("getInstance");
            Object object = method.invoke(new Object[]{});
            Method startInput = inputClass.getMethod("startInput",int.class,boolean.class);
            return (boolean)startInput.invoke(object,1,true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * hide keyboard
     * @return
     */
    public static boolean hideKeyboard(){
        try {
            Class inputClass = Class.forName("ohos.miscservices.inputmethod.InputMethodController");
            Method method = inputClass.getMethod("getInstance");
            Object object = method.invoke(new Object[]{});
            Method stopInput = inputClass.getMethod("stopInput",int.class);
            return (boolean)stopInput.invoke(object,1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }




}
